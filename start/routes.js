'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route');

Route.group(() => {
 Route.get('/', 'CommonController.getIndex').as('index');
 Route.get('/logout', 'CommonController.logout').as('logout');
})
  .middleware('auth');
Route.get('/login', 'CommonController.getLogin')
  .as('getLogin');
Route.post('/login', 'CommonController.postLogin')
  .as('postLogin')
  .validator('User/Login');
Route.get('/register', 'UserController.getRegister')
  .as('register.get');
Route.post('/register', 'UserController.postRegister')
  .as('register.post')
  .validator('User/Register');
