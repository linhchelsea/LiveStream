function singleCall (userId) {
  socket.emit('callPrivate', { user_id: +userId }, (data) => {
    console.log(data);
  });
  $('#callAudioSend').modal({
    show: true,
  });
  $('#btnCancel').click(() => {
    // emit cancel call
    socket.emit('cancelCall', { user_id: +userId });
    // hide modal
    $('#callAudioSend').modal('hide');
  });
}

function openModal (data) {
  $('#btnAccept').click(() => { // accept call
    console.log('accept');
  });
  $('#btnReject').click(() => { // reject call
    console.log('reject');
  });
  $('#callAudio').modal({
    show: true,
  });
  $('#userName').text(data.user.email);
}

function cancelCall (data) {
  // hide call modal
  $('#callAudio').modal('hide');
  // show modal cancel call

}
