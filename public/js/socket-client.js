socket.emit('getUsersOnline', {}, (data) => {
  data.data.forEach((userId) => {
    $(`#status_${userId}`).html('<strong style="color:green;">*</strong>');
  });
});
// get status user online
socket.on('userOnline', (data) => {
  $(`#status_${data.user_id}`).html('<strong style="color:green;">*</strong>');
});

socket.on('userOffline', (data) => {
  $(`#status_${data.user_id}`).html('');
});

socket.on('callPrivate', (data) => {
  console.log(data);
  // open modal
  openModal(data);
});

socket.on('cancelCall', (data) => {
  cancelCall(data);
});
