module.exports = {
  E_INVALID_SESSION: {
    error: 'E_INVALID_SESSION',
    message: 'E_INVALID_SESSION',
  },
  email_existed: {
    error: 1,
    message: 'email_existed',
  },
  register_fail: {
    error: 2,
    message: 'register_fail',
  },
  email_or_password_not_correct: {
    error: 3,
    message: 'email_or_password_not_correct',
  },
};
