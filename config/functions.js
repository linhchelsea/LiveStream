'use strict';

module.exports = {
  callPrivate: 'callPrivate',
  cancelCall: 'cancelCall',
};
