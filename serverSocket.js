const io = require('socket.io')(3000);

const CallController = use('./app/Controllers/Ws/CallController');
const sockets = {};
io.on('connection', (socket) => {
  sockets[+socket.handshake.query.userId] = socket.id;
  const callController = new CallController({ socket });
  /**
   * ----------------------------------------
   *             CALL CONTROLLER            *
   * ----------------------------------------
   */
  callController.callPrivate({ io, sockets });
  callController.cancelCall({ io, sockets });

  // emit user online
  io.emit('userOnline', { user_id: +socket.handshake.query.userId });

  // cache list user online
  socket.on('disconnect', () => {
    const userId = +socket.handshake.query.userId;
    io.emit('userOffline', { user_id: userId });
    // remove outto socketConnections
    delete sockets[+socket.handshake.query.userId];
  });

  socket.on('getUsersOnline', (data, fn) => {
    if(fn instanceof Function) {
      const userIds = Object.keys(sockets);
      fn({
        status: 200,
        error: 0,
        data: userIds,
      });
    }
  });
});

io.on('disconnect', (socket) => {
  console.log(socket.id);
});
