'use strict';

const User = use('App/Models/User');
const RedisHelper = use('App/Helpers/RedisHelper');
class UserRepository {
  /**
   * create new user
   * @param email
   * @param password
   * @returns {*}
   */
  register({ email, password }) {
    return User.findOrCreate({
      email,
      password,
    });
  }

  /**
   * get all user not include me
   * @param userId
   * @returns {*}
   */
  getUsers (userId) {
    return User.query().whereNot('id', userId);
  }

  /**
   * get user info
   * @param userId
   * @returns {Promise.<*>}
   */
  async getUserInfo (userId) {
    let user = await RedisHelper.hgetall(`user_${userId}`);
    if (!user) {
      user = await User.find(userId);
      if (user) { // store redis
        user = user.toJSON();
        await RedisHelper.hmset(`user_${userId}`, user);
      }
    }
    return user;
  }
}

module.exports = UserRepository;
