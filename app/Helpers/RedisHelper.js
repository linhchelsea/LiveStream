'use strict';

const Redis = use('Redis');

class RedisHelper {
  /**
   * check exist
   * @param key
   * @returns {*}
   */
  static exists (key) {
    return Redis.exists(key);
  }

  /**
   * delete key
   * @param key
   */
  static del (key) {
    Redis.del(key);
  }

  /**
   * add key (type = set)
   * @param key
   * @param data
   */
  static sadd (key, data) {
    Redis.sadd(key, data);
  }

  /**
   * remove key (type = set)
   * @param key
   * @param data
   */
  static srem (key, data) {
    Redis.srem(key, data);
  }

  /**
   * get data (type = set)
   * @param key
   * @returns {Promise.<*>}
   */
  static async smembers (key) {
    const isExisted = await Redis.exists(key);
    if (isExisted) {
      return Redis.smembers(key);
    }
    return null;
  }

  /**
   * add key (type = hash)
   * @param key
   * @param data
   */
  static hmset (key, data) {
    Redis.hmset(key, data);
  }

  /**
   * get data (type = hash)
   * @param key
   * @returns {Promise.<*>}
   */
  static async hgetall (key) {
    const isExisted = await Redis.exists(key);
    if (isExisted) {
      return Redis.hgetall(key);
    }
    return null;
  }
}
module.exports = RedisHelper;
