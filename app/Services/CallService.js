'use strict';

const UserRepository = use('App/Repositories/UserRepository');
const Config = use('Config');
const functions = Config.get('functions');
class CallService {
  constructor () {
    this.userRepo = new UserRepository();
  }

  async callPrivate ({ io, sockets, data, userId, fn }) {
    if (fn instanceof Function) {
      fn({
        status: 200,
        data: null,
        message: 'success',
        error: 0,
      });
    }
    // get information of sender
    const user = await this.userRepo.getUserInfo(userId);
    const dataEmit = {
      functionName: functions.callPrivate,
      user,
    };
    io.to(sockets[data.user_id]).emit('callPrivate', dataEmit);
  }

  async cancelCall ({ io, sockets, data, userId, fn }) {
    if (fn instanceof Function) {
      fn({
        status: 200,
        data: null,
        message: 'success',
        error: 0,
      });
    }
    // get information of sender
    const user = await this.userRepo.getUserInfo(userId);
    const dataEmit = {
      functionName: functions.cancelCall,
      user,
    };
    io.to(sockets[data.user_id]).emit('cancelCall', dataEmit);
  }
}

module.exports = CallService;
