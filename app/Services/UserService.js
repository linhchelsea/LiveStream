'use strict';

const UserRepository = use('App/Repositories/UserRepository');
class UserService {
  constructor() {
    this.userRepo = new UserRepository();
  }

  /**
   * register
   * @param email
   * @param password
   * @returns {*}
   */
  register({ email, password }) {
    return this.userRepo.register({ email, password });
  }

  /**
   * login
   * @param email
   * @param password
   * @returns {*}
   */
  login({ email, password }) {
    return this.userRepo.login({ email, password });
  }

  /**
   * get users not include me
   * @param userId
   * @returns {*}
   */
  getUsers (userId) {
    return this.userRepo.getUsers(userId);
  }

  /**
   * get user info
   * @param userId
   * @returns {Promise.<*>}
   */
  getUserInfo (userId) {
    return this.userRepo.getUserInfo(userId);
  }
}

module.exports = UserService;
