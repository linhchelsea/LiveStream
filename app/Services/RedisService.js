'use strict';

const Redis = use('Redis');

class RedisService {
  // check exists
  exists (key) {
    return Redis.exists(key);
  }

  // delete key
  del (key) {
    return Redis.del(key);
  }

  // add key (type = set)
  sadd (key, data) {
    return Redis.sadd(key, data);
  }

  // remove key (type = set)
  srem (key, data) {
    return Redis.srem(key, data);
  }

  // get data (type = set)
  async smembers (key)  {
    const isExisted = await Redis.exists(key);
    if (isExisted) {
      return Redis.smembers(key);
    }
    return null;
  }
// add key (type = hash)
  hmset (key, data) {
    return Redis.hmset(key, data);
  }

  // get data (type = hash)
  async hgetall (key)  {
    const isExisted = await Redis.exists(key);
    if (isExisted) {
      return Redis.hgetall(key);
    }
    return null;
  }
}

module.exports = RedisService;
