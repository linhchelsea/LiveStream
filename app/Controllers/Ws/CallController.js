'use strict';

const UserService = use('App/Services/UserService');
const CallService = use('App/Services/CallService');

class CallController {
  constructor ({ socket, request }) {
    this.socket = socket;
    this.request = request;
    //------------------------------------------------
    this.userService = new UserService();
    this.callService = new CallService();
  }

  /**
   * call private
   * @param io
   */
  callPrivate ({ io, sockets }) {
    this.socket.on('callPrivate', async (data, fn) => {
      const userId = +this.socket.handshake.query.userId;
      this.callService.callPrivate({
        io,
        sockets,
        data,
        userId,
        fn,
      });
    });
  }

  cancelCall ({ io, sockets }) {
    this.socket.on('cancelCall', async (data, fn) => {
      const userId = +this.socket.handshake.query.userId;
      this.callService.cancelCall({
        io,
        sockets,
        data,
        userId,
        fn,
      });
    });
  }
}

module.exports = CallController;
