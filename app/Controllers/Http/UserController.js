'use strict';

const UserService = use('App/Services/UserService');
const Config = use('Config');
const errors = Config.get('errors');
const success = Config.get('success');
class UserController {
  constructor () {
    this.userService = new UserService();
  }

  getRegister({ view }) {
    return view.render('pages.register');
  }

  async postRegister({ request, session, response }) {
    const { email, password } = request.only(['email', 'password']);
    const registerStatus = await this.userService.register({ email, password });
    if (!registerStatus) {
      session.withErrors({
        error: errors.register_fail,
      });
    } else {
      session.flash({
        success: success.register_success,
      });
    }
    return response.route('getLogin');
  }
}

module.exports = UserController;
