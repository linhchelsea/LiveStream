'use strict';

const UserService = use('App/Services/UserService');
const Config = use('Config');
const errors = Config.get('errors');
const success = Config.get('success');
class CommonController {
  constructor () {
    this.userService = new UserService();
  }

  /**
   * show login view
   * @param view
   * @returns {Route}
   */
  getLogin({ view }) {
    return view.render('pages.login');
  }

  /**
   * post login
   * @param request
   * @param response
   * @param session
   * @param auth
   * @returns {Promise.<*>}
   */
  async postLogin({ request, response, session, auth }) {
    const { email, password } = request.only(['email', 'password']);
    try {
      await auth.attempt(email, password);
      return response.route('index');
    } catch (err) {
      session.withErrors({
        error: errors.email_or_password_not_correct,
      });
      return response.redirect('back');
    }
  }

  /**
   * show index view
   * @param view
   * @param auth
   * @returns {Promise.<Route>}
   */
  async getIndex ({ view, auth }) {
    const userId = auth.user.id;
    const users = await this.userService.getUsers(userId);
    return view.render('index', { users });
  }

  async logout({ auth, response }) {
    await auth.logout();
    return response.route('getLogin');
  }
}

module.exports = CommonController;
