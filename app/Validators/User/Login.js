'use strict';

class Login {
  get rules () {
    return {
      email: 'required|email',
      password: 'required|max:60',
    };
  }

  get messages () {
    return {
      'email.required': 'email_is_required',
      'email.email': 'email_is_invalid',
      'password.required': 'password_is_required',
      'password.max': 'password_is_too_long',
    };
  }
  async fails (errorMessages) {
    this.ctx.session.withErrors({
      error: errorMessages[0],
    });
    return this.ctx.response.redirect('back');
  }
}

module.exports = Login;
