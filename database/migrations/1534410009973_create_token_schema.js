'use strict';

const Schema = use('Schema');

class CreateTokenSchema extends Schema {
  up () {
    this.create('tokens', (table) => {
      table.bigIncrements();
      table.bigInteger('user_id').unsigned().references('id').inTable('users');
      table.timestamps();
    })
  }

  down () {
    this.dropIfExists('tokens');
  }
}

module.exports = CreateTokenSchema;
