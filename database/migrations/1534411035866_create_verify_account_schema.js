'use strict';

const Schema = use('Schema');

class CreateVerifyAccountSchema extends Schema {
  up () {
    this.create('verify_accounts', (table) => {
      table.bigIncrements();
      table.bigInteger('user_id').unsigned().references('id').inTable('users');
      table.string('token').notNullable();
      table.boolean('status').default(0);
      table.timestamps();
    })
  }

  down () {
    this.drop('verify_accounts');
  }
}

module.exports = CreateVerifyAccountSchema;
