'use strict';

const Schema = use('Schema');

class CreateUserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.bigIncrements();
      table.string('username', 20).notNullable().default('');
      table.string('email', 254).notNullable().unique();
      table.string('password', 60).notNullable();
      table.string('accept_language', 3).default('en');
      table.timestamps();
    })
  }

  down () {
    this.dropIfExists('users');
  }
}

module.exports = CreateUserSchema;
